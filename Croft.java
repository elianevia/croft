package EVA;
import robocode.*;
import java.awt.Color;

public class Croft extends Robot
{

	public void run() {
		setColors(Color.black, Color.blue, Color.red);
		setScanColor(Color.yellow);

		while(true) {
			// Replace the next 4 lines with any behavior you would like
			double i = (double) (50+Math.random()*500);
			double k = (double) (1+Math.random()*180);
			ahead(i);
			turnRight(k);	
			turnGunRight(180);
					
			
		}
	}
	// verificar alvo
	public void alvo(double Ver) {
		double A=getHeading()+Ver-getGunHeading();
		if (!(A > -180 && A <= 180)) {
			while (A <= -180) {
				A += 360;
			}
			while (A > 180) {
				A -= 360;
			}
		}
		turnGunRight(A);
	}

	// Quando o robô vê outro robô
	public void onScannedRobot(ScannedRobotEvent e) {
		
		alvo(e.getBearing());
		if (e.getDistance() < 50 && getEnergy() > 15) {
			fire(5);																																																																																																																																																										
		} if (e.getDistance() < 50 && getEnergy() < 15) {
 			fire(3);
			
		} else {
			fire(2);
		}
		scan();	
	}

	// robô colide com o outro																																																																																																					 
	public void onHitRobot(HitRobotEvent e) {
	
		alvo(e.getBearing());
	
	}

	// Quando é atingido por uma bala
	public void onHitByBullet(HitByBulletEvent e) {
		
		ahead(100);
		turnRight(180);
		ahead(50);
	}
	
	// Quando colide com uma parede
	public void onHitWall(HitWallEvent e) {
	
		double x = getX();
		double y = getY();
		double n = getBattleFieldWidth();
		double o = getBattleFieldHeight();
		
		if ((x==n) || (x==0)) {
			ahead(50);
		}
		if ((y==o) || (y==0)) {
			back(40);
		}
	}	
}
