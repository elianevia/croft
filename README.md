# Robô Croft.

 O Croft é um robô inteligente com um jogo de cores marcantes que busca visualizar o seu alvo e 
 atacar de forma precisa, possui movimentos condicionados para situações encontradas.

## Pontos fortes
	- Movimentação;
	- Distinção da força do tiro a depender da distância do adversário. 

## Pontos fracos
	- Quando o robô esta próximo de algum alvo ele gasta muita energia.
	- A procura do alvo nem sempre está precisa, para que o tiro seja certeiro.
	
## Aprendizado

1.	Utilizar as estruturas de repetição e de decisão para a criação de estratégia;
2.	Aprender novos comondos e bibliotecas existentes no robocode;
3.	Perceber as possibilidades de aprendizado da utilização de ponteiros de forma simples na
construção de um robô simples e consequentemente as infinitas possibilidades da construção
de robores mais complexos.

